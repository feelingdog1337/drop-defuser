#include <amxmodx>
#include <amxmisc>
#include <cstrike>
#include <fakemeta>
#include <hamsandwich>

#define CLASSNAME_DFSR	"item_thighpack"

new g_defuser, bool:can_user_pick[33];

public plugin_init(){
	register_plugin("Drop defuser", "0.1", "Notepad++");
	register_clcmd("drop", "client_cmdDrop");
	register_event("HLTV", "event_newround", "a", "1=0", "2=0");
	RegisterHam(Ham_Touch, CLASSNAME_DFSR, "defPlyTouch");
	
	g_defuser = engfunc(EngFunc_AllocString, CLASSNAME_DFSR);
}

public client_putinserver(id){
	can_user_pick[id] = true;
}

public client_cmdDrop(id){
	if(!is_user_connected(id)) return PLUGIN_CONTINUE;
	if(get_user_weapon(id) == CSW_KNIFE && cs_get_user_team(id) == CS_TEAM_CT){
		if(cs_get_user_defuse(id)){
			new defkit = engfunc(EngFunc_CreateNamedEntity, g_defuser);
			if(!defkit) return PLUGIN_HANDLED;
			
			static Float:ply_origin[3], Float:throwVel[3];
			pev(id, pev_origin, ply_origin);
			set_pev(defkit, pev_spawnflags, SF_NORESPAWN);
			set_pev(defkit, pev_origin, ply_origin);
			dllfunc(DLLFunc_Spawn, defkit);
			
			velocity_by_aim(id, 400, throwVel);
			set_pev(defkit, pev_velocity, throwVel);
			
			cs_set_user_defuse(id, 0);
			can_user_pick[id] = false;
			set_task(0.1, "canUserPickDefuser_allow", 12034149+id);
			return PLUGIN_HANDLED;
		}
	}
	return PLUGIN_CONTINUE;
}

public canUserPickDefuser_allow(id_task){
	new id = id_task - 12034149;
	can_user_pick[id] = true;
}

public defPlyTouch(defkit_ent, id){
	if(!is_user_connected(id)) return PLUGIN_HANDLED;
	if(cs_get_user_team(id) != CS_TEAM_CT) return PLUGIN_HANDLED;
	if(!can_user_pick[id]) return HAM_SUPERCEDE;
	return PLUGIN_CONTINUE;
}

public event_newround() {
	new defkit = engfunc(EngFunc_FindEntityByString, -1, "classname", CLASSNAME_DFSR);
	while(defkit){
		engfunc(EngFunc_RemoveEntity, defkit);
		defkit = engfunc(EngFunc_FindEntityByString, defkit, "classname", CLASSNAME_DFSR);
	}
	return PLUGIN_CONTINUE;
}